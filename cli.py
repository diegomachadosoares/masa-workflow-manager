import argparse
import subprocess

'''
configure_cli function configure cli's options and defaults
'''


def configure_cli():
    proc = subprocess.Popen('nproc', stdout=subprocess.PIPE)
    nproc = int(proc.stdout.read())
    parser = argparse.ArgumentParser(description="MASA workflow manager.")
    parser.add_argument('--reference',
                        '--ref',
                        '--reference-sequence',
                        '--ref-seq',
                        help="Reference sequence to be compared to.",
                        required=False)
    parser.add_argument('--max-threads',
                        '--threads',
                        help="A list with the number of threads that will used to execute MASA-OpenMP.",
                        type=int,
                        nargs='*',
                        default=1,
                        required=False)
    parser.add_argument('--workflows',
                        '--nworkflows',
                        help="The number of workflows to be executed concurrently.",
                        type=int,
                        default=1,
                        required=False)
    parser.add_argument('--concurrency',
                        '--parallel',
                        help="The maximum number of processes to be executed concurrently.",
                        type=int,
                        nargs='*',
                        default=[3 * nproc],
                        required=False)
    parser.add_argument('--binary',
                        '--executable',
                        help="Binary/executable file to be executed",
                        required=False)
    parser.add_argument('--options',
                        help="Options to be passed as arguments to the binary.",
                        nargs='*',
                        required=False)
    parser.add_argument('--input-type',
                        help="Input type. Either local or S3.",
                        required=False)
    parser.add_argument('--input-bucket',
                        help="Inputs bucket name.",
                        required=False)
    parser.add_argument('--input-path',
                        help="Input directory path works for local and S3 types.",
                        default="inputs",
                        required=False)
    parser.add_argument('--output-type',
                        help="Output type. Either local or S3.",
                        required=False)
    parser.add_argument('--output-bucket',
                        help="Outputs bucket name.",
                        required=False)
    parser.add_argument('--output-path',
                        help="Output directory path. Works for local and S3 types.",
                        default="outputs",
                        required=False)
    parser.add_argument('--sequences',
                        '--seq',
                        help="Sequences to be compared with the reference sequence",
                        nargs='*',
                        required=False)
    parser.add_argument('--config',
                        '--config-file',
                        '-c',
                        '-f',
                        help="Configuration file. If specified all other options will be ignored...",
                        required=False)
    args = parser.parse_args()
    return args
