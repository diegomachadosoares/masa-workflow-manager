#!/usr/bin/env python3

import multiprocessing
import os
import pandas as pd
import subprocess
import shutil
import time
import pathlib
# import memory_profiler

import cli
import file
import config

DEBUG = True
DEBUG_LEVEL = 1

'''
Execute one comparison using MASA-OpenMP
'''


def exec_masa(masa_bin, masa_opts, env, input_path, output_path, sequence1, sequence2):
    # TODO - Remove me!
    # Cleaning up temporary files from previous executions (if they exists).
    # This should only be used for testing purposes!!!
    if os.path.exists(output_path):
        shutil.rmtree(output_path)
    os.makedirs(output_path)
    os.chdir(output_path)

    time_started = time.time()
    # Execute MASA in a subprocess
    if DEBUG and DEBUG_LEVEL > 0:
        if masa_opts is not None:
            result = subprocess.Popen([masa_bin,
                                       masa_opts,
                                       pathlib.Path(input_path).joinpath(sequence1),
                                       pathlib.Path(input_path).joinpath(sequence2)],
                                      env=env)
        else:
            result = subprocess.Popen([masa_bin,
                                       pathlib.Path(input_path).joinpath(sequence1),
                                       pathlib.Path(input_path).joinpath(sequence2)],
                                      env=env)

        # mem = max(memory_profiler.memory_usage(proc=result))
    else:
        if masa_opts is not None:
            result = subprocess.Popen([masa_bin,
                                       masa_opts,
                                       pathlib.Path(input_path).joinpath(sequence1),
                                       pathlib.Path(input_path).joinpath(sequence2)],
                                      env=env,
                                      stdout=subprocess.DEVNULL,
                                      stderr=subprocess.DEVNULL)
        else:
            result = subprocess.Popen([masa_bin,
                                       pathlib.Path(input_path).joinpath(sequence1),
                                       pathlib.Path(input_path).joinpath(sequence2)],
                                      env=env,
                                      stdout=subprocess.DEVNULL,
                                      stderr=subprocess.DEVNULL)

        # mem = max(memory_profiler.memory_usage(proc=result))

    # Wait for the subprocess to finish before continue
    result.wait()
    while True:
        if result.poll() is not None:
            break
    if result.returncode != 0:
        shutil.rmtree(output_path)
        return -1
    time_finished = time.time()

    shutil.rmtree(output_path)

    print("Started: {}".format(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time_started))))
    print("Finihed: {}".format(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time_finished))))
    print("Elapsed time {}s".format(round(time_finished - time_started, 3)))
    # print("Maximum memory used: {} MiB".format(round(mem, 3)))
    return time_finished - time_started  # , mem


'''
This function execute entire MASA Workflow
'''


def exec_masa_workflow(workflow_config):
    times = dict()

    # If output_path doesn't exist, we should create it!
    if os.path.exists(workflow_config['output_path']):
        os.chdir(workflow_config['output_path'])
    else:
        os.makedirs(workflow_config['output_path'])
        os.chdir(workflow_config['output_path'])

    for sequence in workflow_config['sequences']:
        times[sequence] = exec_masa(workflow_config['masa_bin'],
                                    workflow_config['masa_opts'],
                                    workflow_config['env'],
                                    workflow_config['input_path'],
                                    pathlib.Path(workflow_config['output_path']).joinpath(sequence),
                                    workflow_config['compare_input'],
                                    sequence)
    return times


def prepare_masa_workflow(workflow_config):
    output = dict()
    env = workflow_config['env']
    for n in workflow_config['num_threads']:
        if DEBUG:
            print("Executing Workflow " + str(workflow_config['workflow_id']) + " with " + str(n) + " threads.")
        if not os.path.exists(pathlib.Path(workflow_config['output_path'])):
            os.makedirs(pathlib.Path(workflow_config['output_path']))
        meta = open(pathlib.Path(workflow_config['output_path']).joinpath("workflow-" + str(n) + "-threads.meta"), 'w')
        meta.write("Workflow start time: " + time.strftime("%a, %d %b %Y %H:%M:%S +0000\n", time.localtime()))
        start = time.time()
        env["OMP_NUM_THREADS"] = str(n)
        workflow_config['env'] = env
        output[str(n)] = exec_masa_workflow(workflow_config)
        '''
        # Commented out to temporarily disable memory profiling
        file.write_to_csv(pathlib.Path(workflow_config['output_path']).joinpath(str(n) + "-threads.csv"),
                          "sequence," + str(n) + ",max_mem",
                          output[str(n)])
        '''
        file.write_to_csv(pathlib.Path(workflow_config['output_path']).joinpath(str(n) + "-threads.csv"),
                          "sequence," + str(n), output[str(n)])
        finish = time.time()
        meta.write("Workflow finish time: " + time.strftime("%a, %d %b %Y %H:%M:%S +0000\n", time.localtime()))
        meta.write("Concurrency: " + str(n) + "\n")
        meta.write("Workflow duration: " + str(finish - start) + "\n")
        meta.close()

    df = pd.DataFrame(output)
    df.to_csv(pathlib.Path(workflow_config['output_path']).joinpath("workflow.csv"))


def main():
    """
    # Amdahl
    threads = {2: [1, 2, 3, 4, 5],  # large
               4: [1, 2, 3, 4, 5, 6, 7, 8, 9],  # xlarge
               8: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 14, 15, 16, 17],  # 2xlarge
               16: [1, 2, 6, 7, 8, 9, 10, 12, 14, 15, 16, 17, 18, 20, 24, 28, 30, 31, 32, 33, 34],  # 4xlarge
               36: [1, 2, 16, 17, 18, 19, 20, 26, 32, 34, 35, 36, 37, 38, 40, 48, 54, 64, 72],  # 9xlarge
               48: [1, 2, 20, 22, 23, 24, 25, 26, 32, 36, 40, 44, 46, 47, 48, 49, 50, 54, 60, 64, 72, 80, 94, 95, 96,
                    97, 98],  # 12xlarge
               72: [1, 2, 18, 24, 34, 35, 36, 37, 38, 48, 64, 70, 71, 72, 73, 74, 96, 128, 142, 143, 144, 145, 146],
               # 18xlarge
               96: [1, 2, 18, 24, 36, 46, 47, 48, 49, 50, 64, 72, 94, 95, 96, 97, 98, 128, 144, 190, 191, 192, 193,
                    194]}  # 24xlarge
    # Gustafson
    threads = {1: [1],  # c6g.medium
               2: [1],  # large
               4: [1],  # xlarge
               8: [1],  # 2xlarge
               16: [1],  # 4xlarge
               36: [1],  # 9xlarge
               48: [1],  # 12xlarge
               72: [1],  # 18xlarge
               96: [1]}  # 24xlarge
    """

    args = cli.configure_cli()
    workflows = config.configure_workflows(args)
    jobs = []
    for workflow in workflows:
        p = multiprocessing.Process(target=prepare_masa_workflow, args=(workflow,))
        jobs.append(p)
        p.start()
    for job in jobs:
        job.join()


if __name__ == '__main__':
    main()
