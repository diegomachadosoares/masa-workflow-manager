awscli==1.22.46
boto3==1.20.46
botocore==1.23.46
jmespath==0.10.0
memory-profiler==0.60.0
numpy==1.22.1
pandas==1.3.5
psutil==5.9.0
python-dateutil==2.8.2
pytz==2021.3
PyYAML==5.4
s3transfer==0.5.0
six==1.16.0
urllib3==1.26.8
