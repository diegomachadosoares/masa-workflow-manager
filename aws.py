import os

'''
get_inputs_from_s3 function uses awscli system command to download input files from an s3 bucket.
'''


def get_inputs_from_s3(region, bucket_name, input_path, dest_dir):
    cmd = 'aws s3 sync s3://' + bucket_name + '/' + input_path + ' ' + dest_dir
    print(cmd)
    return os.system(cmd)


