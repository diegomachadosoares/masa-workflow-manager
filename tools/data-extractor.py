#!/usr/bin/env python3

import os
import re
from pathlib import Path

BASE_DIR = Path(__file__).resolve(strict=True).parent

def extractdata():
    acc = 0
    nw = 0
    for i in range(10):
        for w in [1, 2, 4, 8, 16, 32]:
            d = Path(Path(BASE_DIR).joinpath(str(i))).joinpath(str(w) + "w")
            # print("\n" + str(d) + "\n")
            dirlist = os.listdir(d)
            for directory in dirlist:
                # print(str(Path(d).joinpath(directory)) + ":\n")
                filelist = os.listdir(Path(d).joinpath(directory))
                for filename in filelist:
                    # print("Filename: " + filename)
                    if filename == 'workflow-2-threads.meta':
                        with open(Path(Path(str(d)).joinpath(str(directory))).joinpath(str(filename)), 'r') as f:
                            for line in  f:
                                if re.search("duration", line):
                                    #print(line.split(' ')[2])
                                    acc = acc + float((line.split(' ')[2]).rstrip("\n"))
                                    nw = nw + 1
            # print(str(acc))
            # print(str(nw))
            print(str(int(acc / nw)))
            acc = 0
            nw = 0


def main():
    extractdata()


if __name__ == '__main__':
    main()
