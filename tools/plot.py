#!/usr/bin/env python3

import matplotlib.pyplot as plot
import pandas as pd

# metal12 = pd.read_csv("./Metal_12_Summary.csv")

# metal12.plot(kind="bar")

metal12 = pd.DataFrame({
    'N Threads': [1, 2, 4, 8, 12, 16, 24, 32, 36, 40, 48, 64],
    'Runtime': [3058, 1768, 1080, 736, 704, 780, 704, 891, 878, 811, 801, 758],
    'SpeedUp': [1, "1,730", "2,831", "4,154", "4,341", "3,923", "4,347", "3,434", "3,483", "3,770", "3,816", "4,036"],
    'Efficiency': [1, 0.865, 0.708, 0.519, 0.362, 0.245, 0.181, 0.107, 0.097, 0.094, 0.080, 0.063]
})
pd.set_option("display.max.columns", None)

# METAL 12
metal12.plot.bar(x='N Threads', y='Runtime', rot=0)
# plot.title("$S_{200}$ average runtime per comparison")
plot.xlabel("Number of OpenMP threads")
plot.ylabel("Average runtime per comparison (miliseconds)")
plot.savefig("fig/12metal-runtime.png")
# plot.show()

metal12.plot.bar(x='N Threads', y='Efficiency', rot=0)
# plot.title("$S_{200}$ average runtime per comparison")
plot.xlabel("Number of OpenMP threads")
plot.ylabel("Average runtime per comparison (miliseconds)")
plot.savefig("fig/12metal-eff.png")
# plot.show()

# METAL 20
metal20 = pd.DataFrame({
    'N Threads': [1, 2, 4, 8, 16, 20, 32, 40, 60, 64, 80],
    'Runtime': [2835, 1556, 920, 629, 519, 506, 456, 444, 417, 420, 494],
    'Efficiency': [1, 0.911, 0.770, 0.563, 0.341, 0.280, 0.194, 0.160, 0.113, 0.105, 0.072]
})
metal20.plot.bar(x='N Threads', y='Runtime', rot=0)
# plot.title("$S_{200}$ average runtime per comparison")
plot.xlabel("Number of OpenMP threads")
plot.ylabel("Average runtime per comparison (miliseconds)")
plot.savefig("fig/20metal-runtime.png")
# plot.show()

metal20.plot.bar(x='N Threads', y='Efficiency', rot=0)
# plot.title("$S_{200}$ average runtime per comparison")
plot.xlabel("Number of OpenMP threads")
plot.ylabel("Average runtime per comparison (miliseconds)")
plot.savefig("fig/20metal-eff.png")
# plot.show()

c59xlarge = pd.DataFrame({
    'N Threads': [1, 2, 4, 8, 16, 18, 32, 36, 40, 54, 64, 72],
    'Runtime': [1.438, 801, 485, 317, 260, 253, 231, 293, 237, 284, 307, 352],
    'Efficiency': [1, 0.897, 0.741, 0.567, 0.346, 0.315, 0.195, 0.136, 0.152, 0.094, 0.073, 0.057]
})
c59xlarge.plot.bar(x='N Threads', y='Runtime', rot=0)
# plot.title("$S_{200}$ average runtime per comparison")
plot.xlabel("Number of OpenMP threads")
plot.ylabel("Average runtime per comparison (miliseconds)")
plot.savefig("fig/c59xlarge-runtime.png")
# plot.show()

c59xlarge.plot.bar(x='N Threads', y='Efficiency', rot=0)
# plot.title("$S_{200}$ average runtime per comparison")
plot.xlabel("Number of OpenMP threads")
plot.ylabel("Average runtime per comparison (miliseconds)")
plot.savefig("fig/c59xlarge-eff.png")
# plot.show()

