#!/usr/bin/env bash

MASA_WORKFLOW_BUCKET='masa-workflow'

MASA_INPUTS_BUCKET="${MASA_WORKFLOW_BUCKET}/inputs"
MASA_OUTPUTS_BUCKET="${MASA_WORKFLOW_BUCKET}/outputs2"

LOCAL_BASEDIR=$(pwd)
LOCAL_INPUTS_DIR="${LOCAL_BASEDIR}/inputs"
LOCAL_OUTPUT_DIR="${LOCAL_BASEDIR}"

INSTANCE_TYPE=$(curl http://169.254.169.254/latest/meta-data/instance-type)
DISTRO='ubuntu'
COMPILER='gcc'
OUTPUT_BUCKET="${MASA_OUTPUTS_BUCKET}/memory/${INSTANCE_TYPE}/${DISTRO}/${COMPILER}/gustafson/bigger-sequences"

TELEGRAM_TOKEN='1489921474:AAEKRAIGRG5E9vtCD9EtaSaC3qdnV_NwB80'
TELEGRAM_CHAT='142684352'

[[ -d "${LOCAL_BASEDIR}" ]] && cd "${LOCAL_BASEDIR}" || exit

function get_inputs_from_s3() {
  [[ -d "${LOCAL_INPUTS_DIR}" ]] && rm -rf "${LOCAL_INPUTS_DIR}"
  aws s3 sync "s3://${MASA_INPUTS_BUCKET}/sars-cov2-bigger-seq/" "${LOCAL_INPUTS_DIR}/"
}

function run_masa() {
  for size in {30,60,120,240,480,960}; do
    echo -e "\n"
    for i in {0..11}; do
      python3 masa-workflow.py --ref-seq "ref-wuhan-${size}k.fa" --seq "ref-MT012098.1-${size}k.fa" --output-dir "./outputs-${size}k/${i}/" --nworkflows "1" --threads "1" --options='--verbose=0'
    done
  done
}

function put_outputs_to_s3() {
  for size in {30,60,120,240,480,960}; do
    aws s3 sync "${LOCAL_OUTPUT_DIR}/outputs-${size}k/" "s3://${OUTPUT_BUCKET}/outputs-${size}k/"
  done
  cat /proc/cpuinfo >/tmp/cpuinfo
  aws s3 cp /tmp/cpuinfo "s3://${OUTPUT_BUCKET}/cpuinfo-$(date +%F)"
  aws s3 cp ./mem.txt "s3://${OUTPUT_BUCKET}/mem-$(date +%F)"
}

function send_report_to_telegram() {
  chmod +x ./tools/telegram
  ./tools/telegram -t "${TELEGRAM_TOKEN}" -c "${TELEGRAM_CHAT}" "$1"
}

send_report_to_telegram "MASA started running on ${INSTANCE_TYPE} at $(date +%R) of $(date +%F)."
get_inputs_from_s3
run_masa
put_outputs_to_s3
send_report_to_telegram "MASA finished running on ${INSTANCE_TYPE} at $(date +%R) of $(date +%F). The results are in in the bucket ${OUTPUT_BUCKET}"
sudo shutdown -h now

exit 0
