#!/usr/bin/env bash

shopt -s extglob

MASA_OUTPUTS_BUCKET='masa-workflow/outputs'

LOCAL_BASEDIR=$(pwd)
LOCAL_OUTPUTS_DIR="${LOCAL_BASEDIR}"

DISTRO='ubuntu'
COMPILER='gcc'

function get_outputs() {
  for instance in {large,xlarge,2xlarge,4xlarge,9xlarge,12xlarge}; do
    aws --profile cnpq s3 sync "s3://${MASA_OUTPUTS_BUCKET}/c5.${instance}/${DISTRO}/${COMPILER}/bigger-sequences/" \
      "${LOCAL_OUTPUTS_DIR}/c5.${instance}/${DISTRO}/${COMPILER}/bigger-sequences/"
  done
}

function showTimes() {
  echo "1,2,4,8,16,18,24,36,48,64,72,96" >/tmp/output.csv
  for instance in {large,xlarge,2xlarge,4xlarge,9xlarge,12xlarge}; do
    for size in {30k,60k,120k,240k,480k,960k}; do
      for i in {0..5}; do
        for t in {1,2,4,8,16,18,24,36,48,64,72,96}; do
          cd "${LOCAL_OUTPUTS_DIR}/c5.${instance}/${DISTRO}/${COMPILER}/bigger-sequences/outputs-${size}/${i}/1w/" || exit
          dir=$(ls .) && cd "${dir}" || exit
          file="workflow-${t}-threads.meta"
          [[ -f ${file} ]] && con=$(grep -Ei '(concurrency)' "${file}")
          [[ -f ${file} ]] && dur=$(grep -Ei '(duration)' ${file})
          echo "${con}: ${dur}"
        done
      done
    done
  done
}

function getTimes() {
  for instance in {c5.large,c5.xlarge,c5.2xlarge,c5.4xlarge,c5.9xlarge,c5.12xlarge}; do
    for size in {30k,60k,120k,240k,480k,960k}; do
      for i in {0..11}; do
        cd "${LOCAL_OUTPUTS_DIR}/${instance}/${DISTRO}/${COMPILER}/bigger-sequences/outputs-${size}/${i}/1w/" || exit
        dir=$(ls .) && cd "${dir}" || exit
        cat workflow.csv >>"${LOCAL_OUTPUTS_DIR}/${instance}/${DISTRO}/${COMPILER}/bigger-sequences/outputs-${size}/workflow-${size}.csv"
      done
      # cat "${LOCAL_OUTPUTS_DIR}/${instance}/${DISTRO}/${COMPILER}/bigger-sequences/outputs-${size}/workflow.csv" >> "${LOCAL_OUTPUTS_DIR}/${instance}/${DISTRO}/${COMPILER}/bigger-sequences/workflow.csv"
    done
  done
  for instance in {c5.large,c5.xlarge,c5.2xlarge,c5.4xlarge,c5.9xlarge,c5.12xlarge}; do
    for size in {30k,60k,120k,240k,480k,960k}; do
      cd "${LOCAL_OUTPUTS_DIR}/${instance}/${DISTRO}/${COMPILER}/bigger-sequences/outputs-${size}/" || exit
      cat "workflow-${size}.csv" >>"${LOCAL_OUTPUTS_DIR}/${instance}/workflow-${instance}.csv"
    done
  done
  for instance in {c5.large,c5.xlarge,c5.2xlarge,c5.4xlarge,c5.9xlarge,c5.12xlarge}; do
    sed -n '3~2!p' "${LOCAL_OUTPUTS_DIR}/${instance}/workflow-${instance}.csv" >"${LOCAL_OUTPUTS_DIR}/${instance}/workflow-${instance}-clean.csv"
    cat "${LOCAL_OUTPUTS_DIR}/${instance}/workflow-${instance}-clean.csv" | tr -s ',' '\t' | tr -s '.' ',' >"${LOCAL_OUTPUTS_DIR}/${instance}/workflow-${instance}-clean.tsv"
  done
}

function cleanUp() {
  for instance in {c5.large,c5.xlarge,c5.2xlarge,c5.4xlarge,c5.9xlarge,c5.12xlarge}; do
    for size in {30k,60k,120k,240k,480k,960k}; do
      rm -rf "${LOCAL_OUTPUTS_DIR}/${instance}/${DISTRO}/${COMPILER}/bigger-sequences/outputs-${size}/workflow-${size}.csv"
      rm -rf "${LOCAL_OUTPUTS_DIR}/${instance}/workflow-${instance}-clean.csv"
      rm -rf "${LOCAL_OUTPUTS_DIR}/${instance}/workflow-${instance}-clean.tsv"
    done
  done
}

# get_outputs
# showTimes
getTimes

exit 0
