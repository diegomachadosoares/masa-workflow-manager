package main

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/ec2"
	. "gitlab.com/diegomachadosoares/masa-workflow-manager/tools"
)

func main() {

	instanceType := []string{"c5.large", "c5.xlarge", "c5.2xlarge", "c5.4xlarge", "c5.9xlarge", "c5.12xlarge", "c5.18xlarge", "c5.24xlarge"}
	imageId := "ami-089418482a7f21909" // MASA-OpenMP Ubuntu Server 20.04 GCC (v2)

	fmt.Println("Getting EC2 session...")
	sess, err := CreateSession("cnpq", "us-east-1")
	if err != nil {
		panic(err)
	}

	ec2svc := ec2.New(sess)
	createParams := &ec2.RunInstancesInput{}
	fmt.Println("----")
	for _, instance := range instanceType {
		createParams = &ec2.RunInstancesInput{
			// ImageId:      aws.String("ami-0103810de755bd0fc"), // My Ubuntu Server 20.04 LTS (HVM), SSD Volume Type
			//ImageId:      aws.String("ami-0885b1f6bd170450c"), // Standard Ubuntu Server 20.04 LTS (HVM), SSD Volume Type
			ImageId:      aws.String(imageId),
			InstanceType: aws.String(instance),
			MinCount:     aws.Int64(1),
			MaxCount:     aws.Int64(1),
			KeyName:      aws.String("diego"),
			SecurityGroupIds: []*string{
				aws.String("sg-00ea5a598dc30ed2a"),
			},
		}
		_, err := ec2svc.RunInstances(createParams)
		if err != nil {
			panic(err)
		}
	}
	params := &ec2.DescribeInstancesInput{
		Filters: []*ec2.Filter{
			&ec2.Filter{
				Name: aws.String("instance-state-name"),
				Values: []*string{
					aws.String("running"),
					aws.String("pending"),
				},
			},
		},
	}
	fmt.Println("Listing created instances...")
	result := DescribeInstances(sess, params)
	PrintInstances(result)
}
