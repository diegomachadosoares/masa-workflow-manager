import boto3
from pathlib import Path


def readInputListFromFile(filepath, filename):
    inputList = []
    with open(Path(filepath).joinpath(filename), 'r') as f:
        inputList.append(f.readline())
        return inputList


def readInputListFromS3(bucketname, filename):
    inputList = []
    s3_resource = boto3.resource('s3')
    s3_resource.Object(bucketname, filename).download_file('/tmp/' + filename)
    with open(Path('/tmp' + filename).joinpath(filename), 'r') as f:
        inputList.append(f.readline())
        return inputList
