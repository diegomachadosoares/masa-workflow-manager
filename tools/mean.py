#!/usr/bin/env python3

import pandas as pd

data = pd.read_csv('./workflow_summ_clean.csv')

means = data.groupby(['seq']).mean()

means.to_csv('./workflow_means.csv')
