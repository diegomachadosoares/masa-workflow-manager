#!/usr/bin/env bash

PACKAGE_DEPENCENCIES=('git' 'vim' 'python3' 'python3-pip')
PYTHON_DEPENDENCIES=('memory_profiler' 'pandas' 'ansible' 'awscli')
MASA_ANSIBLE_ROLE_URL='https://gitlab.com/diegomachadosoares/ansible-role-masa-openmp.git'
MASA_ANSIBLE_ROLE_DIR='/etc/ansible/roles/ansible-role-masa-openmp'

function install_dependencies() {
  apt update
  apt install -y "${PACKAGE_DEPENCENCIES[@]}"
  pip3 install "${PYTHON_DEPENDENCIES[@]}"
}

function install_masa() {
  git clone "${MASA_ANSIBLE_ROLE_URL}" "${MASA_ANSIBLE_ROLE_DIR}"
  [[ -d "${MASA_ANSIBLE_ROLE_DIR}" ]] && cd "${MASA_ANSIBLE_ROLE_DIR}" || exit
  ansible-playbook tests/test.yml
}

install_dependencies
install_masa